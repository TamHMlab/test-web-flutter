import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:test_web/cham_cong/test_cham_cong.dart';

import 'cham_cong/cham_cong.dart';
import 'chat/chat_view.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Chat Firebase',
      initialRoute: '/',
      routes: {
        '/': (context) => MyHomePage(),
        '/sign-up': (context) => SignUp(),
        '/chat': (context) =>Chat(),
        '/cham_cong': (context) => TestCC()
      },
    );
  }
}

class MyHomePage extends StatefulWidget {

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController name = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Welcome"),
        ),
        body: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [
                  Colors.blue[200],
                  Colors.blue[300],
                  Colors.blue[400],
                  Colors.blue[500],
                  Colors.blue[300],
                ],
              )
          ),
          child: Center(
            child: Container(
              height: MediaQuery.of(context).size.height-300,
              width: 400,
              child: Card(
                elevation: 20,
                shadowColor: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 50,bottom: 50),
                      height: 60,
                      width: 60,
                      child: CircleAvatar(
                        backgroundColor: Colors.white,
                        backgroundImage: AssetImage(
                          "images/maxresdefault.jpg",
                        ),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Colors.grey[300],
                      ),
                      margin: EdgeInsets.only(left: 35,right: 35),
                      child: TextFormField(
                        controller: name,
                        textAlignVertical: TextAlignVertical.center,
                        decoration: InputDecoration(
                          hintText: "Enter your name",
                            focusColor: Colors.red,
                            prefixIcon: Icon(
                                Icons.person_outline_outlined
                            ),
                            border: InputBorder.none
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 35,right: 35,top: 35),
                      child: InkWell(
                        splashColor: Colors.red,
                        onTap: (){
                          Navigator.pushNamed(context, '/cham_cong', arguments: name.text);
                          //Navigator.pushNamed(context, '/chat', arguments: name.text );
                        },
                        child: Ink(
                            height: 45,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                gradient: LinearGradient(
                                  begin: Alignment.topLeft,
                                  end: Alignment.bottomRight,
                                  colors: [
                                    Colors.purple[600],
                                    Colors.purple[400],
                                    Colors.purple[400],
                                    Colors.purple[600],
                                  ],
                                )
                            ),
                            child: Center(
                              child: Text("LOGIN",
                                style: TextStyle(
                                    fontSize: 15,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w900
                                ),
                              ),
                            )
                        ),
                      ),
                    ),
                    Spacer(),
                    Padding(
                      padding: EdgeInsets.only(right: 35, left: 35,bottom: 20),
                      child: RichText(
                        text: TextSpan(
                            children: [
                              TextSpan(
                                text: "Not a member? ",
                              ),
                              TextSpan(
                                  recognizer: new TapGestureRecognizer()..onTap = () => Navigator.pushNamed(context, '/sign-up'),
                                  text: "Sign up now",
                                  style: TextStyle(
                                    color: Colors.purple,
                                    decoration: TextDecoration.underline,
                                  )
                              )
                            ]
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        )
    );
  }
}

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(child: Text("Sign up")),
        color: Colors.deepPurple,
      ),

    );
  }
}
