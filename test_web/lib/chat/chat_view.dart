import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:intl/intl_browser.dart';

class Chat extends StatefulWidget {
  @override
  _ChatState createState() => _ChatState();
}

class _ChatState extends State<Chat> {

  final db = FirebaseFirestore.instance;
  TextEditingController mess = TextEditingController();
  ScrollController controller = new ScrollController();
  Stream<QuerySnapshot> users = FirebaseFirestore.instance
      .collection('Chat')
      .orderBy('date')
      .snapshots(includeMetadataChanges: true);
  @override
  // void dispose() {
  //   // db.disableNetwork();
  //   super.dispose();
  // }
  // void initState() {
  //   print('kee');
  //   super.initState();
  // }
  @override
  Widget build(BuildContext context) {
    final String args =
    ModalRoute.of(context).settings.arguments as String;
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: true,
      bottomNavigationBar: BottomAppBar(
        child: Container(
          height: 50,
          width: MediaQuery.of(context).size.width,
          child: TextFormField(
            decoration: InputDecoration(
                suffixIcon: IconButton(
                    icon: Icon(Icons.send),
                    onPressed: () {
                      if(mess.text!=null && mess.text!="" && mess.text.isNotEmpty){
                        print(mess.text);
                        FirebaseFirestore.instance.collection("Chat").add({
                          "name": args,
                          "mess": mess.text,
                          "date": DateTime.now().millisecondsSinceEpoch,
                        }).then((value) => {
                          print(value),
                          print("added"),
                          mess.clear(),
                          controller.jumpTo(controller.position.maxScrollExtent)
                        });
                      }
                    })),
            onFieldSubmitted: (value) {
              if(value!=null && value!="" && value.isNotEmpty){
                print("tapped");
                FirebaseFirestore.instance.collection("Chat").add({
                  "name": args,
                  "mess": value,
                  "date": DateTime.now().millisecondsSinceEpoch,
                }).then((value) => {
                  print(value),
                  print("added"),
                  mess.clear(),
                  controller.jumpTo(controller.position.maxScrollExtent)
                });
              }
            },
            controller: mess,
          ),
        ),
      ),
      body: Container(
        child: StreamBuilder<QuerySnapshot>(
          stream: users,
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasData) {
              print(snapshot.data.docs.length);
              return ListView.builder(
                controller: controller,
                shrinkWrap: true,
                itemCount:snapshot.data.docs.length,
                itemBuilder: (context, index) {
                  DocumentSnapshot ds =
                  snapshot.data.docs[index];
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(left: 10),
                        child: Row(
                          children: [
                            Text(
                              ds['name']??"Dat cai ten vao",
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontWeight: FontWeight.w300,
                                  fontSize: 12
                              ),
                            ),
                            Text(
                              convert(ds['date'])??"",
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontWeight: FontWeight.w300,
                                  fontSize: 12
                              ),
                            ),
                          ],
                        ),
                      ),

                      Container(
                        margin: EdgeInsets.only(left: 15,right: 5, bottom: 25),
                        decoration: BoxDecoration(
                            color: Colors.grey[300],
                            borderRadius: BorderRadius.circular(10)),
                        child: Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Text(ds['mess'],
                            style: TextStyle(
                                color: Colors.black
                            ),
                          ),
                        ),
                      )
                    ],
                  );
                },
              );
            } else if (snapshot.hasError) {
              print("${snapshot.error} errrrrr");
              return Center(child: Text("somthing went wrong"));
            } 
            return Container();
          },
        ),
      ),
    );
  }
  String convert(int time){
    DateTime date = new DateTime.fromMillisecondsSinceEpoch(time);
    var format = new DateFormat("Hm");
    var dateString = format.format(date);
    return " -    "+dateString;
  }
}
