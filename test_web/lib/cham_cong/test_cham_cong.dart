import 'package:carousel_slider/carousel_slider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';


class TestCC extends StatefulWidget {
  final String name;

  const TestCC({Key key, this.name}) : super(key: key);
  @override
  _TestCCState createState() => _TestCCState();
}

class _TestCCState extends State<TestCC> {

  List<Color> colors = [ Colors.black, Colors.teal, Colors.yellow, Colors.red, Colors.purpleAccent];
  List<String> emo = ["😂","😊","🤣","😍","😒"];
  var now = DateTime.now();
  Stream<DocumentSnapshot> chamCong = FirebaseFirestore.instance
      .collection('ChamCong').doc('Tam').collection(DateTime.now().month.toString()).doc(DateTime.now().day.toString()).snapshots();
  var db = FirebaseFirestore.instance.collection("ChamCong").doc('Tam');
  bool monthExist = false;
  bool dayExist = false;
  
  

  @override
  void initState() {
    super.initState();
    try{
      FirebaseFirestore.instance.
      collection("ChamCong")
          .doc('Tam')
          .collection(DateTime.now().month.toString())
          .doc(DateTime.now().day.toString()).get().then((value) => {
         value.exists?print('lol'):add()
      });
    } catch(err){
      print(err);
    }
  }

  @override
  Widget build(BuildContext context) {
    final String args =
    ModalRoute.of(context).settings.arguments as String;
    return Scaffold(
      appBar: AppBar(
        leading: Padding(
          padding: EdgeInsets.all(5),
          child: Lottie.network(
              'https://assets5.lottiefiles.com/animated_stickers/lf_tgs_QYn7Hf.json'
          ),
        ),
        title: Text('Welcome ${args??"stranger"}, today is: ${now.day}/${now.month}/${now.year}'),
      ),
      body: SingleChildScrollView(
        child:Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            StreamBuilder(
              stream: chamCong,
              builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot){
                if(snapshot.hasError){
                  return Center(
                    child: Text("Error"),
                  );
                }
                else if(snapshot.hasData){
                  return Card(
                    elevation: 5,
                    child: Container(
                      height: 50,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 20),
                            child: Text('Hoàng Minh Tâm',
                              style: TextStyle(
                                fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 15,right: 15),
                            width: 1,
                            color: Colors.grey,
                          ),
                          snapshot.data['check in']==null?
                          Container(
                            height: 30,
                            child: ElevatedButton(
                                onPressed:(){
                                  _checkIn();
                                },
                                child: Center(
                                  child: Text('Check in'),
                                )
                            ),
                          ):SizedBox(),
                          Expanded(
                            child: Center(
                              child: Text(
                                snapshot.data['check in']==null?
                                "haven't check in yet"
                                    :"Checked in at: ${convertTimestamp(snapshot.data['check in'])}",
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold
                                ),
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 15,right: 15),
                            width: 1,
                            color: Colors.grey,
                          ),
                          snapshot.data['check out']==null?
                          Container(
                            height: 30,
                            child: ElevatedButton(
                                onPressed:(){
                                   _checkOut();
                                },
                                child: Center(
                                  child: Text('Checkout'),
                                )
                            ),
                          )
                              :SizedBox(),
                          Expanded(
                            child: Center(
                              child: Text(
                                snapshot.data['check out']==null?
                                "haven't check out yet"
                                    :"Checked out at: ${convertTimestamp(snapshot.data['check out'])}",
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold
                                ),
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 15,right: 15),
                            width: 1,
                            color: Colors.grey,
                          ),
                          SizedBox(width: 50),
                          (snapshot.data['check out']!=null && snapshot.data['check in']!=null)?
                              Expanded(
                                child: Center(
                                  child: Text(
                                      "You have worked ${cc(snapshot.data['check out']-snapshot.data['check out'])}today",
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold
                                    ),
                                  ),
                                ),
                              ):SizedBox()
                        ],
                      ),
                    ),
                  );
                }
                return Container(
                  height: MediaQuery.of(context).size.height,
                  child: Center(
                      child: Container(
                        height: 60,
                        width: 60,
                        child: CircularProgressIndicator()
                      ),
                  ),
                );
              },
            ),
            Container(
              margin: EdgeInsets.only(top: 150),
              height: 50,
              child: CarouselSlider.builder(
              itemCount: 5,
              options: CarouselOptions(
                aspectRatio: 2,
                enlargeCenterPage: true,
                autoPlay: true,
                autoPlayInterval: Duration(milliseconds: 2000)
              ),
              itemBuilder: (ctx, index, realIdx) {
                return Container(
                  width: 50,
                  child: Center(
                    child: Text(
                      emo[index],
                      style: TextStyle(
                        fontSize: 30
                      ),
                    ),
                  ),
                );
              }),
            ),
          ],
        )
      ),
    );
  }
  
  _checkIn(){
    FirebaseFirestore.instance.
    collection("ChamCong")
        .doc('Tam')
        .collection(DateTime.now().month.toString())
        .doc(DateTime.now().day.toString()).update({
      'check in':DateTime.now().millisecondsSinceEpoch,
    }).then((value) => {
      print('added'),
    });
  }

  _checkOut(){
    FirebaseFirestore.instance.
    collection("ChamCong")
        .doc('Tam')
        .collection(DateTime.now().month.toString())
        .doc(DateTime.now().day.toString()).update({
      'check out':DateTime.now().millisecondsSinceEpoch,
    }).then((value) => {
      print('checked out'),
    });
  }

  add(){
    FirebaseFirestore.instance.
    collection("ChamCong")
        .doc('Tam')
        .collection(DateTime.now().month.toString())
        .doc(DateTime.now().day.toString()).set({
      'check in':null,
      'check out': null,
      'worked time':null
    }).then((value) => {
      print('added'),
    });
  }

  addMonth(){
    db.set({
      'created at':now
    }).then((value) => {
      add()
    });
  }

  workedTime(int workedTime){
    FirebaseFirestore.instance.
    collection("ChamCong")
        .doc('Tam')
        .collection(DateTime.now().month.toString())
        .doc(DateTime.now().day.toString()).update({
      'worked time':workedTime,
    });
  }
  
  

  String convertTimestamp(int ts){
    var format = DateFormat("hh:mm    dd/MM/yyyy");
    var dt = DateTime.fromMillisecondsSinceEpoch(ts);
    return format.format(dt);
  }
  String cc(int haha){
    double date = DateTime.fromMillisecondsSinceEpoch(haha).minute<60?DateTime.fromMillisecondsSinceEpoch(haha).minute:(DateTime.fromMillisecondsSinceEpoch(haha).minute)/60;
    String hihi = DateTime.fromMillisecondsSinceEpoch(haha).minute<60 ?"minute":"hour";
    return "$date $hihi ";
  }
  int kaka(int hic){
    int rs = DateTime.fromMillisecondsSinceEpoch(hic).minute;
    return rs;
  }
  
  
}
