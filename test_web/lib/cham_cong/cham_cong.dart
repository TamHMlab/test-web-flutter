import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class ChamCong extends StatefulWidget {
  @override
  _ChamCongState createState() => _ChamCongState();
}

class _ChamCongState extends State<ChamCong> {

  Stream<QuerySnapshot> chamcong = FirebaseFirestore.instance
      .collection('ChamCong')
      .orderBy('date')
      .snapshots(includeMetadataChanges: true);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Chấm công'),
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: null,
        builder:
            (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            print(snapshot.data.docs.length);
            return ListView.builder(
              shrinkWrap: true,
              itemCount:snapshot.data.docs.length,
              itemBuilder: (context, index) {
                DocumentSnapshot ds =
                snapshot.data.docs[index];
                return Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 10),
                      child: Row(
                        children: [
                          Text(
                            ds['name']??"Dat cai ten vao",
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                fontWeight: FontWeight.w300,
                                fontSize: 12
                            ),
                          ),
                          Text(
                            'lk',
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                fontWeight: FontWeight.w300,
                                fontSize: 12
                            ),
                          ),
                        ],
                      ),
                    ),

                    Container(
                      margin: EdgeInsets.only(left: 15,right: 5, bottom: 25),
                      decoration: BoxDecoration(
                          color: Colors.grey[300],
                          borderRadius: BorderRadius.circular(10)),
                      child: Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Text(ds['mess'],
                          style: TextStyle(
                              color: Colors.black
                          ),
                        ),
                      ),
                    )
                  ],
                );
              },
            );
          } else if (snapshot.hasError) {
            print("${snapshot.error} errrrrr");
            return Center(child: Text("somthing went wrong"));
          }
          return Container();
        },
      ),
    );
  }
}
